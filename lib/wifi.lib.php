<?php

class wifi {

    const WLAN_DEVICE = 'wlan0';

    public $ap_list = array();
    public $mikrotik_list = array();
    public $scan_raw;

    function scan(){
        $this->scan_raw = shell_exec('iwlist '.self::WLAN_DEVICE.' scan 2>&1 &');

        $x = 0;
        $y = 0;

        foreach (preg_split('/((\r?\n)|(\r\n?))/', $this->scan_raw) as $line_num => $line) {
            if(preg_match('/'.self::WLAN_DEVICE.' No scan results /', $line)){
                return false;
            }
            if(preg_match('/Cell [0-9][0-9] - Address: /', $line)){
                if(isset($mac_address) && $mac_address != trim(preg_replace('/Cell [0-9][0-9] - Address: /', '', $line))){
                    $this->ap_list[$x] = (object) array('mac_address' => strtolower($mac_address), 'channel' => $channel, 'encryption' => $encryption, 'essid' => $essid, 'mikrotik' => $mikrotik);
                    if($mikrotik == true){
                        $this->mikrotik_list[$y] =  (object) array('mac_address' => strtolower($mac_address), 'channel' => $channel, 'encryption' => $encryption, 'essid' => $essid, 'mikrotik' => $mikrotik);
                        $y++;
                    }
                    $x++;
                }

                unset($channel);
                unset($mac_address);
                unset($encryption);
                unset($essid);

                $mac_address = trim(preg_replace('/Cell [0-9][0-9] - Address: /', '', $line));
            }

            if(isset($mac_address)){
                if(preg_match('/Channel:/', $line)){
                    $channel = trim(preg_replace('/Channel:/', '', $line));
                }

                if(preg_match('/Encryption key:/', $line)){
                    if(trim(preg_replace('/Encryption key:/', '', $line)) == 'on'){
                        $encryption = true;
                    }elseif(trim(preg_replace('/Encryption key:/', '', $line)) == 'off'){
                        $encryption = false;
                    }
                }

                if(preg_match('/ESSID:/', $line)){
                    $essid = trim(trim(preg_replace('/ESSID:/', '', $line)), '"');

                    if(preg_match('/MikroTik-[0-9a-fA-F]{6}/', $line)){
                        $mikrotik = true;
                    }else{
                        $mikrotik = false;
                    }
                }
            }
        }

        return true;
    }
}

?>