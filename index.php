<?php

error_reporting(E_ALL);

require_once('lib/wifi.lib.php');

?><!DOCTYPE html>
<head>
    <title>AP Configurator</title>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <style>
        html, body{
            font-family: "Verdana";
            font-size: 20px;
            font-weight: 300;
        }

        .green {
            color: #6DB031;
        }

        .red {
            color: #ae1f27;
        }

        .entry_wrapper{
            clear: both;
            padding: 25px;
        }

        .left{
            float: left;
        }

        .icon{
            margin-right: 25px;
        }

        .half{
            width: 50%;
        }

        .menu{
            width: 100%;
            margin-bottom: 25px;
        }

        button{
            padding: 15px;
            font-size: 30px;
        }

        .clear{
            clear: both;
        }
    </style>
</head>
<body>
<div class="menu">
    <form>
        <button type="submit" name="action" value="scan">Scan</button>
        <button type="submit" name="action" value="setup">Mikrotik AP's initialisieren</button>
    </form>
</div>
<div>
    <div class="left half">
        <div>Alle AP's</div>
<?php

$wifi = new wifi();
$wifi->scan();

if($_GET['action'] == 'setup'){
    if(is_array($wifi->mikrotik_list)){
        foreach($wifi->mikrotik_list as $index => $value){
            shell_exec($_SERVER['CONTEXT_DOCUMENT_ROOT'].'/setupMikrotik.sh "'.$value->essid.'"');
        }
    }
}

if(is_array($wifi->ap_list)){
    foreach($wifi->ap_list as $index => $value){
?>
<div class="entry_wrapper" id="ap_<?php print $index; ?>">
    <div class="left icon"><i class="fa fa-wifi fa-2x <?php if($value->mikrotik == true){ print 'green'; }else{ print 'red'; } ?>"></i></div>
    <div class="left">
        <div><?php print($value->mac_address); ?></div>
        <div><?php if($value->encryption == true){ print '<i class="fa fa-lock red"></i>'; }else{ print '<i class="fa fa-unlock green"></i>'; } ?> <?php print($value->essid); ?></div>
        <div class="clear"></div>
    </div>
</div>
<?php
    }
}
?>
    </div>
    <div class="left half">
        <div>Mikrotik AP's</div>
<?php
if(is_array($wifi->mikrotik_list)){
    foreach($wifi->mikrotik_list as $index => $value){
        ?>
        <div class="entry_wrapper" id="ap_<?php print $index; ?>">
            <div class="left icon"><i class="fa fa-wifi fa-2x <?php if($value->mikrotik == true){ print 'green'; }else{ print 'red'; } ?>"></i></div>
            <div class="left">
                <div><?php print($value->mac_address); ?></div>
                <div><?php if($value->encryption == true){ print '<i class="fa fa-lock red"></i>'; }else{ print '<i class="fa fa-unlock green"></i>'; } ?> <?php print($value->essid); ?></div>
                <div class="clear"></div>
            </div>
        </div>
    <?php
    }
}
?>
    </div>
</div>
</body>